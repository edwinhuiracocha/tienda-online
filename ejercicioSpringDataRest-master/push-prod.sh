mvn install  -DskipTests
docker rmi ms-tiendap
docker rmi gcr.io/ms-tienda-252915/ms-tiendap:latest
docker build -t ms-tiendap .
docker tag ms-tiendap gcr.io/ms-tienda-252915/ms-tiendap:latest
docker push gcr.io/ms-tienda-252915/ms-tiendap:latest
