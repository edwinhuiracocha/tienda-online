package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.CategoriaRepositorio;
import ec.edu.tecazuay.ejercicio1.controlador.ProductoRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Producto;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/producto")

public class ProductoRes implements Serializable {

    @Autowired
    ProductoRepositorio productoRepositorio;
      @Autowired
    CategoriaRepositorio categoriaRepositorio;

   @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<Producto> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.productoRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<Producto> listar(@ApiIgnore final Pageable page) {
        return this.productoRepositorio.findAll(page);
    }
    
    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("/lfecha")
    public Page<Producto> listarPorFecha(@ApiIgnore final Pageable page) {
        return this.productoRepositorio.ordenarPorFechaActual(page);
    }

    @GetMapping("/{idProducto}")
    public Producto recuperar(@PathVariable Long idProducto) {
        return this.productoRepositorio.getOne(idProducto);
    }

    @PostMapping("")
    public Producto guardar(@RequestBody Producto categoria) {
        return this.productoRepositorio.save(categoria);
    }

    @DeleteMapping("/{idProducto}")
    public void borrar(@PathVariable Long idProducto) {
        this.productoRepositorio.deleteById(idProducto);
    }
        @ResponseBody
    @GetMapping("/categoria/{categoria}")
    public List<Producto> listaProducto(@PathVariable Long categoria) {
        return this.productoRepositorio.recuperarProductoPorCategoria(categoriaRepositorio.getOne(categoria));
    }
    

}
