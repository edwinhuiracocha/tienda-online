package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.StockRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Stock;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/stock")

public class StockRest implements Serializable {

    @Autowired
    StockRepositorio stockRepositorio;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<Stock> listar(@ApiIgnore final Pageable page) {
        return this.stockRepositorio.findAll(page);
    }

    @GetMapping("/{idStock}")
    public Stock recuperar(@PathVariable Long idStock) {
        return this.stockRepositorio.getOne(idStock);
    }

    @PostMapping("")
    public Stock guardar(@RequestBody Stock categoria) {
        return this.stockRepositorio.save(categoria);
    }

    @DeleteMapping("/{idStock}")
    public void borrar(@PathVariable Long idStock) {
        this.stockRepositorio.deleteById(idStock);
    }
}
