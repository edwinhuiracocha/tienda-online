package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.TipoDetalle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TipoDetalleRepositorio extends JpaRepository<TipoDetalle, Long> {

    @Query(value = "select a from TipoDetalle a where a.descripcion like %:text% ")
    Page<TipoDetalle> filtrar(@Param("text") String text, Pageable page);

}
