package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.EmpresaRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Empresa;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/empresa")

public class EmpresaRest {

    @Autowired
    EmpresaRepositorio empresaRepositorio;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<Empresa> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.empresaRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<Empresa> listar(@ApiIgnore final Pageable page) {
        return this.empresaRepositorio.findAll(page);
    }

    @GetMapping("/{idEmpresa}")
    public Empresa recuperar(@PathVariable Long idEmpresa) {
        return this.empresaRepositorio.getOne(idEmpresa);
    }

    @PostMapping("")
    public Empresa guardar(@RequestBody Empresa empresa) {
        return this.empresaRepositorio.save(empresa);
    }

    @DeleteMapping("/{idEmpresa}")
    public void borrar(@PathVariable Long idEmpresa) {
        this.empresaRepositorio.deleteById(idEmpresa);
    }
}
