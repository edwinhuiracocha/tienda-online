package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Movimiento;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MovimientoRepositorio extends JpaRepository<Movimiento, Long> {

    @Query(value = "select a from Movimiento a where a.descripcion like %:text%")
    Page<Movimiento> filtrar(@Param("text") String text, Pageable page);

}
