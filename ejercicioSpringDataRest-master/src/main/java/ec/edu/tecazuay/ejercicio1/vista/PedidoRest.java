package ec.edu.tecazuay.ejercicio1.vista;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.tecazuay.ejercicio1.controlador.PedidoRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Pedido;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import springfox.documentation.annotations.ApiIgnore;

@RestController

@RequestMapping("/pedido")
public class PedidoRest {

    @Autowired
    PedidoRepositorio pedidoRepositorio;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<Pedido> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.pedidoRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<Pedido> listar(@ApiIgnore final Pageable page) {
        return this.pedidoRepositorio.findAll(page);
    }

    @GetMapping("/{idPedido}")
    public Pedido recuperar(@PathVariable Long idPedido) {
        return this.pedidoRepositorio.getOne(idPedido);
    }

    @PostMapping("")
    public Pedido guardar(@RequestBody Pedido categoria) {
        return this.pedidoRepositorio.save(categoria);
    }

    @DeleteMapping("/{idPedido}")
    public void borrar(@PathVariable Long idPedido) {
        this.pedidoRepositorio.deleteById(idPedido);
    }
}
