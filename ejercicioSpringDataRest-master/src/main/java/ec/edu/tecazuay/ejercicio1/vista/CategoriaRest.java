package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.modelo.Categoria;
import io.swagger.annotations.*;
import springfox.documentation.annotations.ApiIgnore;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import ec.edu.tecazuay.ejercicio1.controlador.CategoriaRepositorio;

@RestController
@RequestMapping("/categoria")
public class CategoriaRest implements Serializable {

    @Autowired
    CategoriaRepositorio categoriaRepositorio;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<Categoria> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.categoriaRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<Categoria> listar(@ApiIgnore final Pageable page) {
        return this.categoriaRepositorio.findAll(page);
    }

    @GetMapping("/{idCategoria}")
    @CrossOrigin
    public Categoria recuperar(@PathVariable Long idCategoria) {
        return this.categoriaRepositorio.getOne(idCategoria);
    }

    @PostMapping("")
    @CrossOrigin
    public Categoria guardar(@RequestBody Categoria categoria) {
        return this.categoriaRepositorio.save(categoria);
    }

    @DeleteMapping("/{idCategoria}")
    @CrossOrigin
    public void borrar(@PathVariable Long idCategoria) {
        this.categoriaRepositorio.deleteById(idCategoria);
    }
}
