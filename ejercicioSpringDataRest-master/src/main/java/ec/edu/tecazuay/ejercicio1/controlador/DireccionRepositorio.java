package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Direccion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DireccionRepositorio extends JpaRepository<Direccion, Long> {

    @Query(value = "select a from Direccion a where a.callePrincipal like %:text% or a.calleSecundaria like %:text% ")

    Page<Direccion> filtrar(@Param("text") String text, Pageable page);

}
