package ec.edu.tecazuay.ejercicio1.vista;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ec.edu.tecazuay.ejercicio1.controlador.BodegaRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Bodega;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/bodega")
public class BodegaRest {

    @Autowired
    BodegaRepositorio bodegaRepositorio;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<Bodega> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.bodegaRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<Bodega> listar(@ApiIgnore final Pageable page) {
        return this.bodegaRepositorio.findAll(page);
    }

    @GetMapping("/{idBodega}")
    public Bodega recuperar(@PathVariable Long idBodega) {
        return this.bodegaRepositorio.getOne(idBodega);
    }

    @PostMapping("")
    public Bodega guardar(@RequestBody Bodega idBodega) {
        return this.bodegaRepositorio.save(idBodega);
    }

    @DeleteMapping("/{idBodega}")
    public void borrar(@PathVariable Long idBodega) {
        this.bodegaRepositorio.deleteById(idBodega);
    }
}
