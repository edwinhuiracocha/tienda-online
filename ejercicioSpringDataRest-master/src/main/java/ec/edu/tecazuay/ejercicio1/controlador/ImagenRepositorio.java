package ec.edu.tecazuay.ejercicio1.controlador;

import org.springframework.data.jpa.repository.JpaRepository;
import ec.edu.tecazuay.ejercicio1.modelo.Imagen;

public interface ImagenRepositorio extends JpaRepository<Imagen, Long> {

}
