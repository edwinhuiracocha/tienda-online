package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.TipoDetalleRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.TipoDetalle;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/tipoDetalle")
public class TipoDetalleRest {

    @Autowired
    TipoDetalleRepositorio tipoDetalle;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<TipoDetalle> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.tipoDetalle.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<TipoDetalle> listar(@ApiIgnore final Pageable page) {
        return this.tipoDetalle.findAll(page);
    }

    @GetMapping("/{idTipoDetalle}")
    public TipoDetalle recuperar(@PathVariable Long idTipoDetalle) {
        return this.tipoDetalle.getOne(idTipoDetalle);
    }

    @PostMapping("")
    public TipoDetalle guardar(@RequestBody TipoDetalle categoria) {
        return this.tipoDetalle.save(categoria);
    }

    @DeleteMapping("/{idTipoDetalle}")
    public void borrar(@PathVariable Long idTipoDetalle) {
        this.tipoDetalle.deleteById(idTipoDetalle);
    }
}
