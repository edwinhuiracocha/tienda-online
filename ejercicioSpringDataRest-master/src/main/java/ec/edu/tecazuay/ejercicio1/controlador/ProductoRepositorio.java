package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Categoria;
import ec.edu.tecazuay.ejercicio1.modelo.Producto;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductoRepositorio extends JpaRepository<Producto, Long> {

    @Query(value = "select a from Producto a where a.nombre like %:text% or a.descripcion like %:text% or a.estado like %:text% ")
    Page<Producto> filtrar(@Param("text") String text, Pageable page);

    @Query(value = "select a from Producto a order by a.fechaIngreso desc")
    Page<Producto> ordenarPorFechaActual(Pageable page);

    @Query(value = "select a from Producto a where :categoria member of a.categorias")
    List<Producto> recuperarProductoPorCategoria(@Param("categoria") Categoria categoria);

}
