package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Configuracion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ConfiguracionRepositorio extends JpaRepository<Configuracion, Long> {

    @Query(value = "select a from Configuracion a where a.codigo like %:text% or  a.nombre like %:text%  or  a.descripcion like %:text% or  a.valor like %:text% ")
    Page<Configuracion> filtrar(@Param("text") String text, Pageable page);

}
