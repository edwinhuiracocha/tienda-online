package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Pedido;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PedidoRepositorio extends JpaRepository<Pedido, Long> {

    @Query(value = "select a from Pedido a where a.descripcion like %:text% "
            + "or a.estado like %:text%")
    Page<Pedido> filtrar(@Param("text") String text, Pageable page);

}
