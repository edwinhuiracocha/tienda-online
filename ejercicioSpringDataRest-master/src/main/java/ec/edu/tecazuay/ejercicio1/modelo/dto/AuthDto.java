package ec.edu.tecazuay.ejercicio1.modelo.dto;

public class AuthDto {

    private String usuario;
    private String pass;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

}
