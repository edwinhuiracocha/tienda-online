package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.AcercaDe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AcercaDeRepositorio extends JpaRepository<AcercaDe, Long>{
    
     @Query(value = "select a from AcercaDe a where a.nombre like %:text% or a.cargo like %:text%")
     Page<AcercaDe> filtrar(@Param("text") String text, Pageable page); 
  
    
}
