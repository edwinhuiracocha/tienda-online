package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Contacto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ContactoRepositorio extends JpaRepository<Contacto, Long> {

    @Query(value = "select a from Contacto a where a.telefono like %:text% or a.direccion like %:text%")
    Page<Contacto> filtrar(@Param("text") String text, Pageable page);
}
