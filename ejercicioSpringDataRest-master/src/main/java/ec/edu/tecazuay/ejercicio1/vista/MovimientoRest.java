package ec.edu.tecazuay.ejercicio1.vista;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ec.edu.tecazuay.ejercicio1.controlador.MovimientoRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Movimiento;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.io.Serializable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/movimiento")
public class MovimientoRest implements Serializable {

    @Autowired
    MovimientoRepositorio movimientoRepositorio;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<Movimiento> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.movimientoRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<Movimiento> listar(@ApiIgnore final Pageable page) {
        return this.movimientoRepositorio.findAll(page);
    }

    @GetMapping("/{idMovimiento}")
    public Movimiento recuperar(@PathVariable Long idMovimiento) {
        return this.movimientoRepositorio.getOne(idMovimiento);
    }

    @PostMapping("")
    public Movimiento guardar(@RequestBody Movimiento categoria) {
        return this.movimientoRepositorio.save(categoria);
    }

    @DeleteMapping("/{idMovimiento}")
    public void borrar(@PathVariable Long idMovimiento) {
        this.movimientoRepositorio.deleteById(idMovimiento);
    }
}
