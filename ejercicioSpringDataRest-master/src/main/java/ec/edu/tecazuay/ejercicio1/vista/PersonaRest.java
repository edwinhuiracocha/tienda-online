package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.service.CorreoService;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.tecazuay.ejercicio1.controlador.PersonaRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Persona;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/persona")
public class PersonaRest implements Serializable {

    @Autowired
    PersonaRepositorio personaRepositorio;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<Persona> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.personaRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<Persona> listar(@ApiIgnore final Pageable page) {
        return this.personaRepositorio.findAll(page);
    }

    @GetMapping("/{idPersona}")
    public Persona recuperar(@PathVariable Long idPersona) {
        return this.personaRepositorio.getOne(idPersona);
    }

    @PostMapping("")
    public Persona guardar(@RequestBody Persona categoria) {
        return this.personaRepositorio.save(categoria);
    }

    @DeleteMapping("/{idPersona}")
    public void borrar(@PathVariable Long idPersona) {
        this.personaRepositorio.deleteById(idPersona);
    }

    @Autowired
    CorreoService correoService;

    @GetMapping("/enviarCorreo")
    @ResponseBody
    public void enviarCorreo() {
        this.correoService.sendEmail();
    }
}
