package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.ImagenRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Imagen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/imagen")
public class ImagenRest {
  
       @Autowired
       ImagenRepositorio imagenRepositorio;
     @ResponseBody
    @GetMapping("")
    public Page<Imagen> listar(@ApiIgnore final Pageable page) {
        return this.imagenRepositorio.findAll(page);
    }

    @GetMapping("/{idImagen}")
    public Imagen recuperar(@PathVariable Long idImagen) {
        return this.imagenRepositorio.getOne(idImagen);
    }

    @PostMapping("")
    public Imagen guardar(@RequestBody Imagen imagen) {
        return this.imagenRepositorio.save(imagen);
    }

    @DeleteMapping("/{idImagen}")
    public void borrar(@PathVariable Long idImagen) {
        this.imagenRepositorio.deleteById(idImagen);
    }
}
