package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Perfil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PerfilRepositorio extends JpaRepository<Perfil, Long> {

    @Query(value = "select a from Perfil a where a.tipo like %:text% ")
    Page<Perfil> filtrar(@Param("text") String text, Pageable page);

}
