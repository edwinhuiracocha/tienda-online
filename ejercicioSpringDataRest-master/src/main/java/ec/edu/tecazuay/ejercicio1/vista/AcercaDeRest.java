package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.AcercaDeRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.AcercaDe;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 *
 * @author edwin
 */
@RestController
@RequestMapping("/acercaDe")
public class AcercaDeRest implements Serializable{
    
      @Autowired
      AcercaDeRepositorio acercadeRepositorio;
 @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<AcercaDe> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.acercadeRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<AcercaDe> listar(@ApiIgnore final Pageable page) {
        return this.acercadeRepositorio.findAll(page);
    }

    @GetMapping("/{idAcercaDe}")
    public AcercaDe recuperar(@PathVariable Long idAcercaDe) {
        return this.acercadeRepositorio.getOne(idAcercaDe);
    }

    @PostMapping("")
    public AcercaDe guardar(@RequestBody AcercaDe acercade) {
        return this.acercadeRepositorio.save(acercade);
    }

    @DeleteMapping("/{idAcercaDe}")
    public void borrar(@PathVariable Long idAcercaDe) {
        this.acercadeRepositorio.deleteById(idAcercaDe);
    }
    
    
}
