package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Logo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface LogoRepositorio extends JpaRepository<Logo, Long> {
     @Query(value = "select a from Logo a where a.nombre like %:text%")
    Page<Logo> filtrar(@Param("text") String text, Pageable page);
    
}
