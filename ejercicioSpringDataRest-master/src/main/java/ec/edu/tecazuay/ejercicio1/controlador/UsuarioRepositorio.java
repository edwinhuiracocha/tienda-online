package ec.edu.tecazuay.ejercicio1.controlador;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ec.edu.tecazuay.ejercicio1.modelo.Usuario;

public interface UsuarioRepositorio extends JpaRepository<Usuario, Long> {

    @Query(value = "select a from Usuario a where a.usuario = :usuario")
    Usuario validarUsuario(@Param("usuario") String usuario);
}
