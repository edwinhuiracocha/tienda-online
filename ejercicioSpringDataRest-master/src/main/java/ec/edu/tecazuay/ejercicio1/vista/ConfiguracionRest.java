
package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.ConfiguracionRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Configuracion;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 *
 * @author edwin
 */
@RestController
@RequestMapping("/configuracion")
public class ConfiguracionRest implements Serializable{
    
    @Autowired
    ConfiguracionRepositorio configuracionRepositorio;
    
     @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
     
     @ResponseBody
    @GetMapping("")
    public Page<Configuracion> listar(@ApiIgnore final Pageable page) {
        return this.configuracionRepositorio.findAll(page);
    }

    @GetMapping("/{idConfiguracion}")
    public Configuracion recuperar(@PathVariable Long idConfiguracion) {
        return this.configuracionRepositorio.getOne(idConfiguracion);
    }

    @PostMapping("")
    public Configuracion guardar(@RequestBody Configuracion configuracion) {
        return this.configuracionRepositorio.save(configuracion);
    }

    @DeleteMapping("/{idConfiguracion}")
    public void borrar(@PathVariable Long idConfiguracion) {
        this.configuracionRepositorio.deleteById(idConfiguracion);
    }
    
}
