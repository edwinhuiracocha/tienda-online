package ec.edu.tecazuay.ejercicio1.vista;

import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.tecazuay.ejercicio1.controlador.DetalleProductoRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.DetalleProducto;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@RequestMapping("/detalleProducto")

public class DetalleProductoRest implements Serializable {

    @Autowired
    DetalleProductoRepositorio detalleProductoRepositorio;


    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<DetalleProducto> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.detalleProductoRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<DetalleProducto> listar(@ApiIgnore final Pageable page) {
        return this.detalleProductoRepositorio.findAll(page);
    }

    @GetMapping("/{idDetalleProducto}")
    public DetalleProducto recuperar(@PathVariable Long idDetalleProducto) {
        return this.detalleProductoRepositorio.getOne(idDetalleProducto);
    }

    @PostMapping("")
    public DetalleProducto guardar(@RequestBody DetalleProducto DetalleProducto) {
        return this.detalleProductoRepositorio.save(DetalleProducto);
    }

    @DeleteMapping("/{idDetalleProducto}")
    public void borrar(@PathVariable Long idDetalleProducto) {
        this.detalleProductoRepositorio.deleteById(idDetalleProducto);
    }
}
