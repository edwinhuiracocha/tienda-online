package ec.edu.tecazuay.ejercicio1.vista;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ec.edu.tecazuay.ejercicio1.controlador.DireccionRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Direccion;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/direccion")

public class DireccionRest {

    @Autowired
    DireccionRepositorio direccionRepositorio;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<Direccion> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.direccionRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<Direccion> listar(@ApiIgnore final Pageable page) {
        return this.direccionRepositorio.findAll(page);
    }

    @GetMapping("/{idDireccion}")
    public Direccion recuperar(@PathVariable Long idDireccion) {
        return this.direccionRepositorio.getOne(idDireccion);
    }

    @PostMapping("")
    public Direccion guardar(@RequestBody Direccion direccion) {
        return this.direccionRepositorio.save(direccion);
    }

    @DeleteMapping("/{idDireccion}")
    public void borrar(@PathVariable Long idDireccion) {
        this.direccionRepositorio.deleteById(idDireccion);
    }
}
