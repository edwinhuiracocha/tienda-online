package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Blog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BlogRepositorio extends JpaRepository<Blog, Long> {

    @Query(value = "select a from Blog a where a.nombreProducto like %:text% ")
    Page<Blog> filtrar(@Param("text") String text, Pageable page);

}
