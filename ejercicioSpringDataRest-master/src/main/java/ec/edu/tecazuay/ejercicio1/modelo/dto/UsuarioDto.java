package ec.edu.tecazuay.ejercicio1.modelo.dto;

import java.util.List;

import ec.edu.tecazuay.ejercicio1.modelo.Perfil;

public class UsuarioDto {

    private String email;
    private String usuario;
    private List<Perfil> perfil;
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

	public List<Perfil> getPerfil() {
		return perfil;
	}

	public void setPerfil(List<Perfil> perfil) {
		this.perfil = perfil;
	}

}
