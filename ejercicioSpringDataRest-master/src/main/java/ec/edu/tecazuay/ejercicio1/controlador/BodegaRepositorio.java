package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Bodega;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Page;

public interface BodegaRepositorio extends JpaRepository<Bodega, Long> {

    @Query(value = "select a from Bodega a where a.nombre like %:text% ")
    Page<Bodega> filtrar(@Param("text") String text, Pageable page);

}
