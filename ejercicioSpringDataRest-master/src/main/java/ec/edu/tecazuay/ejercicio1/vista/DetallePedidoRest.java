package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.DetallePedidoRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.DetallePedido;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@RequestMapping("/detallePedido")

public class DetallePedidoRest implements Serializable {

    @Autowired
    DetallePedidoRepositorio detalleRepositorio;
    

  

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<DetallePedido> listar(@ApiIgnore final Pageable page) {
        return this.detalleRepositorio.findAll(page);
    }

    @GetMapping("/{idDetallePedido}")
    public DetallePedido recuperar(@PathVariable Long idDetallePedido) {
        return this.detalleRepositorio.getOne(idDetallePedido);
    }

    @PostMapping("")
    public DetallePedido guardar(@RequestBody DetallePedido categoria) {
        return this.detalleRepositorio.save(categoria);
    }

    @DeleteMapping("/{idDetallePedido}")
    public void borrar(@PathVariable Long idDetallePedido) {
        this.detalleRepositorio.deleteById(idDetallePedido);
    }
}
