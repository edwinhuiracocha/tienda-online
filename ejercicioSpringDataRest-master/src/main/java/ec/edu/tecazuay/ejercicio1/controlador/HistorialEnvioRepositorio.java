package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.HistorialEnvio;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface HistorialEnvioRepositorio extends JpaRepository<HistorialEnvio, Long> {

    @Query(value = "select a from HistorialEnvio a where a.lugarDestino like %:text% or a.lugarFinal like %:text%")
    Page<HistorialEnvio> filtrar(@Param("text") String text, Pageable page);

}
