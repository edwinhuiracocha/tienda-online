package ec.edu.tecazuay.ejercicio1.vista;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import ec.edu.tecazuay.ejercicio1.controlador.UsuarioRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Usuario;
import ec.edu.tecazuay.ejercicio1.modelo.dto.AuthDto;
import ec.edu.tecazuay.ejercicio1.modelo.dto.Respuesta;
import ec.edu.tecazuay.ejercicio1.modelo.dto.UsuarioDto;
import ec.edu.tecazuay.ejercicio1.vista.util.JwtTokenUtil;
import ec.edu.tecazuay.ejercicio1.vista.util.UsuarioToken;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import springfox.documentation.annotations.ApiIgnore;


/**
 *
 * @author juanurgiles
 */
@RestController
@RequestMapping("/usuario")
public class UsuarioRest {


    @Autowired
    private UsuarioRepositorio usuarioRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtTokenUtil jwtutil;

    @PostMapping(value = "/")
    @CrossOrigin
    public Usuario guardar(@RequestBody Usuario usuario) {
        try {
            usuario.setContrasena(this.passwordEncoder.encode(usuario.getContrasena()));
            return usuarioRepository.save(usuario);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    @ResponseBody
    @CrossOrigin
    public void eliminar(@RequestBody Usuario usuario) {
        try {

            usuarioRepository.delete(usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo utilizado para busqueda (Mejora en paginación Ajax)
     *
     * @param pageable
     * @return
     */
    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    /**
     * Returns a {@link Page} of entities meeting the paging restriction
     * provided in the {@code Pageable} object.
     *
     * @param pageable
     * @return a page of entities
     */
    public Page<Usuario> buscar(@ApiIgnore final Pageable pageable) {
        try {

            return usuarioRepository.findAll(pageable);
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    // @ApiOperation(value = "Find partners")
    // @ApiImplicitParams({
    // @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
    // value = "Results page you want to retrieve (0..N)"),
    // @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
    // value = "Number of records per page."),
    // @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string",
    // paramType = "query", value = "Sorting criteria in the format:
    // property(,asc|desc). "
    // + "Default sort order is ascending. " + "Multiple sort criteria are
    // supported.") })
    // @RequestMapping(value = "evaluacionxConvocatoria/busquedageneral/{periodo}",
    // method = RequestMethod.GET)
    // @ResponseBody
    // @CrossOrigin
    /**
     * Returns a {@link Page} of entities meeting the paging restriction
     * provided in the {@code Pageable} object.
     *
     * @param pageable
     * @return a page of entities
     * @PathVariable Long periodo,@PathVariable String jornada,
     */
    // public Page<Usuario> busquedaGeneral(@RequestParam List<Long> texto,
    // @PathVariable Long periodo,
    // @ApiIgnore final Pageable pageable) {
    // try {
    // return usuarioRepository.busquedaGeneral(periodo, texto, pageable);
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // }
    /**
     * Método utilizado para guardar una entidad mediante método post
     *
     * @param entidad
     * @return
     */
    @RequestMapping(value = "/logueo", method = RequestMethod.POST)
    @ResponseBody
    @CrossOrigin
    public Respuesta logueo(@RequestBody AuthDto usuario) {
        try {

            Usuario u = usuarioRepository.validarUsuario(usuario.getUsuario());
            Respuesta r = new Respuesta();
            Gson gson = new Gson();
            if (u!=null && passwordEncoder.matches(usuario.getPass(), u.getContrasena())) {
                r.setUsuario(gson.fromJson(gson.toJson(u), UsuarioDto.class));
                UsuarioToken ut = new UsuarioToken();
                ut.setUsername(u.getUsuario());
                ut.setIdentificacion(u.getUsuario());
               
                r.setValido(true);
                r.setToken(jwtutil.generateToken(ut));
                
            } else {
                r.setValido(false);
            }

            return r;
        } catch (Exception e) {
            e.printStackTrace();

            return null;

        }
    }

//	

}
