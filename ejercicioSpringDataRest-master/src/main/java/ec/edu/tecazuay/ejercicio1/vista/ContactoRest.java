package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.modelo.Contacto;
import io.swagger.annotations.*;
import springfox.documentation.annotations.ApiIgnore;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.tecazuay.ejercicio1.controlador.ContactoRepositorio;
@RestController
@RequestMapping("/contacto")
public class ContactoRest implements Serializable {

    @Autowired
    ContactoRepositorio contactoRepositorio;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<Contacto> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.contactoRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<Contacto> listar(@ApiIgnore final Pageable page) {
        return this.contactoRepositorio.findAll(page);
    }

    @GetMapping("/{idContacto}")
    public Contacto recuperar(@PathVariable Long idContacto) {
        return this.contactoRepositorio.getOne(idContacto);
    }

    @PostMapping("")
    public Contacto guardar(@RequestBody Contacto contacto) {
        return this.contactoRepositorio.save(contacto);
    }

    @DeleteMapping("/{idContacto}")
    public void borrar(@PathVariable Long idContacto) {
        this.contactoRepositorio.deleteById(idContacto);
    }
}
