package ec.edu.tecazuay.ejercicio1.vista;

import ec.edu.tecazuay.ejercicio1.controlador.BlogRepositorio;
import ec.edu.tecazuay.ejercicio1.modelo.Blog;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/blog")
public class BlogRest {

    @Autowired
    BlogRepositorio blogRepositorio;

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @CrossOrigin
    @ResponseBody
    @GetMapping("/filter/{text}")
    public Page<Blog> listarFilter(@PathVariable String text, @ApiIgnore final Pageable page) {
        return this.blogRepositorio.filtrar(text, page);
    }

    @ApiOperation(value = "Find partners")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
        @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
        @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})

    @ResponseBody
    @GetMapping("")
    public Page<Blog> listar(@ApiIgnore final Pageable page) {
        return this.blogRepositorio.findAll(page);
    }

    @GetMapping("/{idBlog}")
    public Blog recuperar(@PathVariable Long idBlog) {
        return this.blogRepositorio.getOne(idBlog);
    }

    @PostMapping("")
    public Blog guardar(@RequestBody Blog blog) {
        return this.blogRepositorio.save(blog);
    }

    @DeleteMapping("/{idBlog}")
    public void borrar(@PathVariable Long idBlog) {
        this.blogRepositorio.deleteById(idBlog);
    }

}
