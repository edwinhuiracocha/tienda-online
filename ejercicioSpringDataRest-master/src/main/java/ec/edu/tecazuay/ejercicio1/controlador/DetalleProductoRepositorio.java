package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.DetalleProducto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DetalleProductoRepositorio extends JpaRepository<DetalleProducto, Long> {

    @Query(value = "select a from DetalleProducto a where a.material like %:text% or a.descripcion like %:text% ")
    Page<DetalleProducto> filtrar(@Param("text") String text, Pageable page);

}
