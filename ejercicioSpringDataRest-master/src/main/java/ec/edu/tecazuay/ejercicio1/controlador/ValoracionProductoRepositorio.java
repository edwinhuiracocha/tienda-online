package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.ValoracionProducto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ValoracionProductoRepositorio extends JpaRepository<ValoracionProducto, Long> {

    @Query(value = "select a from ValoracionProducto a where a.calificacion like %:text%")
    Page<ValoracionProducto> filtrar(@Param("text") String text, Pageable page);

}
