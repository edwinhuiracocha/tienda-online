package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Empresa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EmpresaRepositorio extends JpaRepository<Empresa, Long> {

    @Query(value = "select a from Empresa a where a.nombre like %:text% or a.telefono like %:text% or a.direccion like %:text% or a.ciudad like %:text% ")
    Page<Empresa> filtrar(@Param("text") String text, Pageable page);
}
