package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepositorio extends JpaRepository<Stock, Long> {

}
