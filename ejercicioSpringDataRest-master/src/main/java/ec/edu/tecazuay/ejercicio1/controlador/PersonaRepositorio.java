package ec.edu.tecazuay.ejercicio1.controlador;

import ec.edu.tecazuay.ejercicio1.modelo.Persona;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PersonaRepositorio extends JpaRepository<Persona, Long> {

    @Query(value = "select a from Persona a where a.nombre like %:text%"
            + " or a.apellido like %:text%"
            + " or a.estado like %:text% ")
    Page<Persona> filtrar(@Param("text") String text, Pageable page);

}
